# BDO Efficiency

## Introduction

Ce projet cible la communauté d'un **MMORPG** (_Massively multiplayer online role-playing game_) d'origine coréenne
appelé
**Black Desert Online** (similaire à World of Warcraft dans le principe). <br>
Site officiel : https://www.naeu.playblackdesert.com/en-US/Main/Index

Il a pour but de simplifier la vie des joueurs (Quality of Life - QoL) en leur fournissant une plateforme unique qui
leur permettra de calculer et de sauvegarder en toute simplicité le rendement de leurs activités en jeu.

**BDO Efficiency** s'aide de l'**API** officielle de **Black Desert Online** afin d'afficher les prix des objets du jeu
en temps réel.

## Architecture du projet (3 tiers)

### 3 Tiers

Mon projet se compose de 3 partie (tiers) :

1. Frontend (ReactJS) : https://gitlab.com/vincent.praz/bdoefficiency-frontend
2. Backend (Spring Boot) : https://gitlab.com/vincent.praz/bdoefficiency
3. Données : MySQL

### Schéma de fonctionnement

![FonctionnementSchema.PNG](FonctionnementSchema.PNG)

## Récupération du projet

Afin de récupérer les projets, il suffit de git clone les deux repos back et front ou de dézipper l'archive '.zip'
fournie.

```sh
git clone https://gitlab.com/vincent.praz/bdoefficiency-frontend.git
git clone https://gitlab.com/vincent.praz/bdoefficiency.git
```

_NB: Les repos gitlabs seront mis en public jusqu'à réception du résultat de mon Travail de Bachelor_

## Mise en place - Base de données (MySQL)

### Serveur

Avant de lancer le backend ou le frontend, il vous faudra un serveur MySQL pour la partie Data du projet. Afin de lancer
ce serveur, j'ai choisi d'utiliser **XAMPP** mais vous pouvez utilisez MAMPP ou WAMPP qui permettent également de lancer
un serveur MySQL sur le port **3306**.

Conseils :

- Ouvrez ce programme de préférences en mode Administrateur.
- Si le port 3306 est déjà occupé, chercher dans votre gestionnaire de tâches windows le processus mysql et mettez-y fin
  ou utilisez la command kill suivit de l'identifiant du processus (processId) après l'avoir identifié

### Import des données

Une fois le serveur MySQL lancé, vous pourrez ouvrir votre visualiseur de Base de donnée favoris (**MySQL Workbench**
pour ma part).

Il vous faudra vous connecter à l'instance MySQL sur le port 3306 et importer les données tests via le script
**_BDOEfficiency-data.sql_** qui se trouve à la racine du répertoire du projet backend nommé **_bdoefficiency_**.

**Avec MySQL Workbench**  : _Server > Data Import > Import From Disk > Import from Self-Contained-File >_
**_votre-chemin\bdoefficiency\BDOEfficiency-data.sql_**

Le script s'occupera lui même de créer le schéma de base de données requis et y importer les données nécessaires pour
que vous puissiez tester le projet.

Si vous rencontrez l'erreur suivante lors de l'import du script :

```
#1071 - Specified key was too long; max key length is 768 bytes
```

Suivez le Quickfix#1 du guide https://github.com/directus/v8-archive/issues/1383

Voici les étapes à effectuer si vous ne voulez pas vous rendre sur le guide :

- Ouvrir **MySQL Command Line Client**
- Entrer votre mot de passe
- Entrez les commandes
  ```shell
  set global innodb_large_prefix=ON;
  set global innodb_default_row_format=dynamic;
  ```
- Puis réessayer d'importer les données

**_⚠️ NB : L'utilisateur admin de la base de données doit être root avec un mot de passe vide. <br/>Si cela n'est pas le
cas pour vous, changez-le ou changez les variables d'environnements du projet backend dans
**bdoefficiency\src\main\resources\application.properties** et **rebuildez le projet. <br/> (CF : Marche à suivre pour
build & run le projet en ligne de commande)**_**

### Schéma

Vous pouvez également visualiser le schéma de la base de donnée via l'image à la racine du projet :
**_DatabaseSchema.PNG_**

![DatabaseSchema.PNG](DatabaseSchema.PNG)

## Mise en place - Backend (Spring Boot)

### Prérequis :

- **Tomcat 9.0** installé sur votre machine (http://tomcat.apache.org/tomcat-9.0-doc/index.html)
- Variable d'environnement **JAVA_HOME** qui pointe vers le **JDK
  1.8.0** (https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)

### Exécution du projet

Ouvrez le projet backend avec votre IDE (IntelliJ de Jetbrains pour moi).

Si vous utilisez également IntelliJ vous pourrez Build+Run l'application grace au bouton Start.

Si ce n'est pas le cas, je vous ai préparé une archive **_target/bdoefficiency-0.0.1-SNAPSHOT.war_** qui peut être
exécutée en ligne de commande depuis la racine du projet backend.

```shell
java -jar target/bdoefficiency-0.0.1-SNAPSHOT.war
```

Logs en cas de réussite du lancement de l'application :

```shell
2021-08-27 15:25:34.671 INFO 12364 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2021-08-27 15:25:34.680 INFO 12364 --- [           main] c.z.b.BdoefficiencyApplication           : Started BdoefficiencyApplication in 11.406 seconds (JVM running for 12.211)
```

**⚠️ Laissez ce terminal ouvert.**

**⚠️ Si vous rencontrez une erreur jdbc/mysql lors du lancement, assurez-vous que les informations de connexion à votre
base de données correspondent à celles présentes dans le fichier
_bdoefficiency/src/main/resources/application.properties_. Si vous changez une valeur dans ce fichier, suivez la
rubrique qui suit :
_Marche à suivre pour build & run le projet en ligne de commande_**

### Marche à suivre pour build & run le projet en ligne de commande

**⚠️ Si vous avez réussi à lancer le projet avec les étapes précédentes, cette rubrique n'est pas nécessaire.**

1. Aller dans le répertoire du projet dans un terminal
2. Entrez la commande suivante pour créer une archive exécutable jar/war dans le sous-répertoire "_**target**_":

#### Windows

```shell
mvnw package
```

#### MAC OS:

```shell
./mvnw package
```

3. Exécuter le fichier jar/war qui a été créé avec la commande

```shell
java -jar target/bdoefficiency-0.0.1-SNAPSHOT.war
```

### Tests Unitaires

Vous pouvez également lancer les tests unitaires qui se trouvent dans le répertoire "**_test_**"

## Mise en place - Frontend (ReactJS)

### Prérequis

- npm installé sur votre machine

### Lancement du projet

Depuis votre terminal entrez la commande suivante à la racine du projet (**_bdoefficiency-frontend_**)

```shell
npm install
```

Une fois l'installation des packages terminée, lancez la commande suivante :

```shell
npm run start
```

### Utilisation

Une fois la partie frontend lancée, vous pourrez vous rendre sur : http://localhost:8081/

Vous arriverez sur la page d'accueil de mon application.

Afin de tester mon application, je vous ai mis à disposition 3 utilisateurs :

1. Un utilisateur sans donnée

|  Username | Password  |
|---|---|
| userdemo  | userdemo  |

2. Un utilisateur administrateur ayant accès aux pages d'administrations

|  Username | Password  |
|---|---|
| admin  | admindemo  |

3. Un utilisateur que j'ai utilisé pour insérer les données tests afin d'avoir un rendu réaliste sur les graphiques

|  Username | Password  |
|---|---|
| userdata  | userdata  |

**⚠️ NB : Ce site a été développé et optimisé pour le navigateur Chrome. <br/>Si vous avez des problèmes d'affichages
avec d'autres navigateurs, veuillez passer à Chrome.**

## Documentations

### Javadoc

La documentation java se trouve dans le répertoire du projet backend et est visualisable en ouvrant le fichier
_**bdoefficiency/javadoc/index.html**_ dans votre navigateur

## Librairies utilisées & Références

### Graphiques

- https://reactchartjs.github.io/react-chartjs-2/#/

### Styling

- https://styled-components.com/

### Icones

- https://fontawesome.com/
- https://icons.getbootstrap.com/

### API Black Desert Online

- API URL : https://eu-trade.naeu.playblackdesert.com/Trademarket/
- Documentation non-officielle de l'API : https://developers.veliainn.com/

### Black Desert Online

- https://www.naeu.playblackdesert.com/en-US/Main/Index

### Tutoriels

- https://www.bezkoder.com/

## Contact

Pour toutes questions concernant l'installation, l'exécution ou l'utilisation de mon projet, je suis joignable :

- Par email : praz.vinc@gmail.com
- Par télépone : +41 76 430 24 93
- Via LinkedIn : https://www.linkedin.com/in/vincent-praz/

# Merci et bonne correction !