package com.zelyvanna.bdoefficiency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdoefficiencyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BdoefficiencyApplication.class, args);
    }

}
