package com.zelyvanna.bdoefficiency.repository;

import com.zelyvanna.bdoefficiency.model.Classe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Classe Repository
 */
@Repository
public interface ClasseRepository extends JpaRepository<Classe, Long> {

    /**
     * Finds a Classe by its id
     *
     * @param id Classe's unique identifier
     * @return The Class that has been found
     */
    Classe findById(int id);


    /**
     * Deletes a Classe by its id
     *
     * @param id Class's unique identifier
     */
    void deleteById(int id);


    /**
     * Search for the Classe's names containing the name in parameter
     *
     * @param name name of the Classe to search
     * @return the list of Classe found corresponding to the name
     */
    List<Classe> findByNameContaining(String name);
}
