package com.zelyvanna.bdoefficiency.repository;

import com.zelyvanna.bdoefficiency.model.Grindspot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Grindspot Repository
 */
@Repository
public interface GrindspotRepository extends JpaRepository<Grindspot, Long> {
    /**
     * Search for the grindspots's names containing the name in parameter
     *
     * @param name name of the grindspots to search
     * @return the list of grindspots found corresponding to the name
     */
    List<Grindspot> findByNameContaining(String name);


    /**
     * Finds a grindspot by its id
     *
     * @param id grindspot's unique identifier
     * @return The Grindspot that has been found
     */
    Grindspot findById(int id);


    /**
     * Deletes a grindspot by its id
     *
     * @param id grindspot's unique identifier
     */
    void deleteById(int id);
}
