package com.zelyvanna.bdoefficiency.repository;

import com.zelyvanna.bdoefficiency.model.Grind;
import com.zelyvanna.bdoefficiency.model.Grindspot;
import com.zelyvanna.bdoefficiency.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Grind Session Repository
 */
@Repository
public interface GrindRepository extends JpaRepository<Grind, Long> {

    /**
     * Finds a grindspot by its id
     *
     * @param id grind hour's unique identifier
     * @return The Grind hour that has been found
     */
    Grind findById(int id);


    /**
     * Find grinds by their user
     *
     * @param user User of the grind sessions
     * @return The list of Grinds that have been created by the user
     */
    List<Grind> findByUser(User user);

    /**
     * Find grinds by their user and their grindspot
     *
     * @param user      User of the grind sessions
     * @param grindspot The grindspot where the grind session occurs
     * @return The list of Grinds that have been created by the user and occuring in a specifing grindspot
     */
    List<Grind> findByUserAndGrindspot(User user, Grindspot grindspot);


    /**
     * Finds the 10 last grindspots created (10 biggest ids)
     *
     * @return the 10 last grindspots created (10 biggest ids)
     */
    List<Grind> findTop10ByOrderByIdDesc();

    /**
     * Deletes a grind hour by its id
     *
     * @param id grind hour's unique identifier
     */
    void deleteById(int id);
}
