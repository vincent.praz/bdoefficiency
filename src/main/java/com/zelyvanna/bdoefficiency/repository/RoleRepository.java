package com.zelyvanna.bdoefficiency.repository;


import com.zelyvanna.bdoefficiency.model.ERole;
import com.zelyvanna.bdoefficiency.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Role Repository
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
