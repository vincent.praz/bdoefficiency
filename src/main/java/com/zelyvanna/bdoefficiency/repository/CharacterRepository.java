package com.zelyvanna.bdoefficiency.repository;

import com.zelyvanna.bdoefficiency.model.Character;
import com.zelyvanna.bdoefficiency.model.Classe;
import com.zelyvanna.bdoefficiency.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Character Repository
 */
@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
    /**
     * Search for the Character's names containing the name in parameter
     *
     * @param name name of the Character to search
     * @return the list of Characters found corresponding to the name
     */
    List<Character> findByNameContaining(String name);

    /**
     * Search for the Characters having the user in parameter as owner
     *
     * @param user user of the Character's to search
     * @return the list of Characters found owned by the user
     */
    List<Character> findByUser(User user);


    /**
     * Search for Characters having a specific class
     *
     * @param classe The classe of the character to search for
     * @return the list of Characters found by classe
     */
    List<Character> findByClasse(Classe classe);


    /**
     * Finds a Character by its id
     *
     * @param id Character's unique identifier
     * @return The Character that has been found
     */
    Character findById(int id);


    /**
     * Deletes a Character by its id
     *
     * @param id Character's unique identifier
     */
    void deleteById(int id);
}
