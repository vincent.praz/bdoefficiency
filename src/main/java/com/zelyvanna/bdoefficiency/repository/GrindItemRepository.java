package com.zelyvanna.bdoefficiency.repository;

import com.zelyvanna.bdoefficiency.model.GrindItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Grind Item Repository
 */
@Repository
public interface GrindItemRepository extends JpaRepository<GrindItem, Long> {

    /**
     * Finds a GrindItem by its id
     *
     * @param id grind hour's unique identifier
     * @return The GrindItem that has been found
     */
    GrindItem findById(int id);

    /**
     * Deletes a GrindItem by its id
     *
     * @param id GrindItem's unique identifier
     */
    void deleteById(int id);
}
