package com.zelyvanna.bdoefficiency.repository;

import com.zelyvanna.bdoefficiency.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Item Repository
 */
@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    /**
     * Search for the items's names containing the name in parameter
     *
     * @param name name of the items to search
     * @return the list of items found corresponding to the given name
     */
    List<Item> findByNameContaining(String name);

    /**
     * Finds an item by its id
     *
     * @param id item's unique identifier
     * @return The Item that has been found
     */
    Item findById(int id);

    /**
     * Deletes an item by its id
     *
     * @param id item's unique identifier
     */
    void deleteById(int id);
}
