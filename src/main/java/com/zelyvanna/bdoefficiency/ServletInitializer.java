package com.zelyvanna.bdoefficiency;

import com.zelyvanna.bdoefficiency.security.WebSecurityConfig;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BdoefficiencyApplication.class);
	}

}
