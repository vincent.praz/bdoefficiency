package com.zelyvanna.bdoefficiency.model;

/**
 * Grindspot Averages
 * Average money per hour per grindpot
 */
public class GrindspotAverage {
    /**
     * Grindspot of the Grindspot Average
     */
    private Grindspot grindspot;
    /**
     * Average money per hour on the grindspot
     */
    private int average;

    /**
     * Default GrindspotAverage Constructor
     *
     * @param grindspot grindspot of the average
     * @param average   average money per hour on the grindspot
     */
    public GrindspotAverage(Grindspot grindspot, int average) {
        this.grindspot = grindspot;
        this.average = average;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }

    public Grindspot getGrindspot() {
        return grindspot;
    }

    public void setGrindspot(Grindspot grindspot) {
        this.grindspot = grindspot;
    }
}
