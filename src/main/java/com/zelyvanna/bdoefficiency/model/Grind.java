package com.zelyvanna.bdoefficiency.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Grind Session Entity
 */
@Entity
@Table(name = "grinds")
public class Grind {
    /**
     * Unique identifier of the Grind Session
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * User of the Grind Session
     */
    // Bidirectional many to one
    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    /**
     * Character that was used to during the Grind Session
     */
    @ManyToOne
    @JoinColumn(name = "c_character")
    private Character c_character;

    /**
     * Grindspot of the Grind Session
     */
    @ManyToOne
    @JoinColumn(name = "grindspot")
    private Grindspot grindspot;

    /**
     * Grind Items dropped during the Grind Session
     */
    @OneToMany(mappedBy = "grind", cascade = CascadeType.ALL)
    private Set<GrindItem> grindItems = new HashSet<>();

    /**
     * Duration  of the Grind Session in ms
     */
    @Column(name = "duration")
    private int duration;

    /**
     * Value pack (True if used | false) Marketplace tax reduction
     */
    @Column(name = "value_pack")
    private boolean valuePack;

    /**
     * Kamasylve Blessing (True if active | false) 20% item droprate
     */
    @Column(name = "kamasylve")
    private boolean kamasylve;

    /**
     * Drop event (True if active | false) 50% item droprate
     */
    @Column(name = "drop_event")

    private boolean dropEvent;
    /**
     * Blue Loot Scroll (True if active | false) 100% item droprate + 50% item drop amout
     */
    @Column(name = "blue_scoll")
    private boolean blueScroll;

    /**
     * Yellow Loot Scroll (True if active | false) 100% item droprate + 100% item drop amout
     */
    @Column(name = "yellow_scroll")
    private boolean yellowScroll;

    /**
     * Agris Fever (True if active | false) 100% item drop amount
     */
    @Column(name = "agris")
    private boolean agris;

    /**
     * Tent Buff (True if active | false) 50% item droprate
     */
    @Column(name = "tent")
    private boolean tent;

    /**
     * Node Level
     */
    @Column(name = "node")
    private int node;

    /**
     * Total silver earned
     */
    // Total money calculated with the dropped GrindItem's infos
    @Column(name = "total_silver")
    private int totalSilver;

    /**
     * Empty constructor of grind
     */
    public Grind() {

    }

    /**
     * Grind Session's constructor
     *
     * @param user         Owner of the Grind Session
     * @param grindspot    Grindspot of the Grind Session
     * @param c_character  Character of the Grind Session
     * @param duration     Duration of the Grind Session
     * @param valuePack    Value Pack of the Grind Session
     * @param kamasylve    Kamasylve of the Grind Session
     * @param dropEvent    Drop Event of the Grind Session
     * @param blueScroll   Blue Scroll of the Grind Session
     * @param yellowScroll Yellow Scroll of the Grind Session
     * @param agris        Agris of the Grind Session
     * @param tent         Tenr of the Grind Session
     * @param node         Node of the Grind Session
     */
    public Grind(User user, Grindspot grindspot, Character c_character, int duration, boolean valuePack, boolean kamasylve, boolean dropEvent, boolean blueScroll, boolean yellowScroll, boolean agris, boolean tent, int node) {
        this.user = user;
        this.c_character = c_character;
        this.grindspot = grindspot;
        this.duration = duration;
        this.valuePack = valuePack;
        this.kamasylve = kamasylve;
        this.dropEvent = dropEvent;
        this.blueScroll = blueScroll;
        this.yellowScroll = yellowScroll;
        this.agris = agris;
        this.tent = tent;
        this.node = node;
        this.totalSilver = 0;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Character getC_character() {
        return c_character;
    }

    public void setC_character(Character c_character) {
        this.c_character = c_character;
    }

    public Grindspot getGrindspot() {
        return grindspot;
    }

    public void setGrindspot(Grindspot grindspot) {
        this.grindspot = grindspot;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Set<GrindItem> getGrindItems() {
        return grindItems;
    }

    public void setGrindItems(Set<GrindItem> grindItems) {
        this.grindItems = grindItems;
    }

    public void addGrindItem(GrindItem grindItem) {
        this.grindItems.add(grindItem);
    }

    public void removeGrindItem(GrindItem grindItem) {
        this.grindItems.remove(grindItem);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isValuePack() {
        return valuePack;
    }

    public void setValuePack(boolean valuePack) {
        this.valuePack = valuePack;
    }

    public int getTotalSilver() {
        return totalSilver;
    }

    public void setTotalSilver(int totalSilver) {
        this.totalSilver = totalSilver;
    }

    /**
     * Calculates and returns the total silver gained of the grind session
     *
     * @return totalSilver of the grind session
     */
    public int calculateTotalSilver() {
        int totalSilver = 0;
        // For each grind items
        for (GrindItem grindItem : this.grindItems) {
            // Check if the item is taxed (marketplace tax)
            if (grindItem.getItem().isTaxed()) {
                // We check if the value pack is active
                if (this.valuePack) {
                    // Calculate price with marketplace and value pack reduction
                    totalSilver += (int) Math.round(grindItem.getQuantity() * grindItem.getPrice() * 0.845);
                } else {
                    // Calculate price with marketplace
                    totalSilver += (int) Math.round(grindItem.getQuantity() * grindItem.getPrice() * 0.65);
                }
            } else {
                // Increase total price by the qty * price of the item
                totalSilver += grindItem.getQuantity() * grindItem.getPrice();
            }
        }
        return totalSilver;
    }

    public boolean isKamasylve() {
        return kamasylve;
    }

    public void setKamasylve(boolean kamasylve) {
        this.kamasylve = kamasylve;
    }

    public boolean isDropEvent() {
        return dropEvent;
    }

    public void setDropEvent(boolean dropEvent) {
        this.dropEvent = dropEvent;
    }

    public boolean isBlueScroll() {
        return blueScroll;
    }

    public void setBlueScroll(boolean blueScroll) {
        this.blueScroll = blueScroll;
    }

    public boolean isYellowScroll() {
        return yellowScroll;
    }

    public void setYellowScroll(boolean yellowScroll) {
        this.yellowScroll = yellowScroll;
    }

    public boolean isAgris() {
        return agris;
    }

    public void setAgris(boolean agris) {
        this.agris = agris;
    }

    public boolean isTent() {
        return tent;
    }

    public void setTent(boolean tent) {
        this.tent = tent;
    }

    public int getNode() {
        return node;
    }

    public void setNode(int node) {
        this.node = node;
    }
}
