package com.zelyvanna.bdoefficiency.model;

/**
 * Number of grind sessions per grindspot
 */
public class GrindspotCount {
    /**
     * Grindspot
     */
    private Grindspot grindspot;

    /**
     * Count
     */
    private int count;

    /**
     * Default GrindspotCount Constructor
     *
     * @param grindspot grindspot of the grindspot count
     * @param count     count of the grindspot
     */
    public GrindspotCount(Grindspot grindspot, int count) {
        this.grindspot = grindspot;
        this.count = count;
    }

    public Grindspot getGrindspot() {
        return grindspot;
    }

    public void setGrindspot(Grindspot grindspot) {
        this.grindspot = grindspot;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
