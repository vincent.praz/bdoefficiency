package com.zelyvanna.bdoefficiency.model;

/**
 * Classe Averages
 * Average money per hour per classe
 */
public class ClasseAverage {
    /**
     * Classe of the Classe Average
     */
    private Classe classe;

    /**
     * Average money per hour of the classe
     */
    private int average;

    /**
     * Default ClasseAverage Constructor
     *
     * @param classe  Classe of the Classe Average
     * @param average Average money per hour of the classe
     */
    public ClasseAverage(Classe classe, int average) {
        this.classe = classe;
        this.average = average;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }
}
