package com.zelyvanna.bdoefficiency.model;

/**
 * Number of character per classe
 */
public class ClasseCount {
    /**
     * Classe of the Classe count
     */
    private Classe classe;

    /**
     * Count
     */
    private int count;

    /**
     * Default ClassCount Constructor
     *
     * @param classe Classe of the Classe count
     * @param count  count of the classe
     */
    public ClasseCount(Classe classe, int count) {
        this.classe = classe;
        this.count = count;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
