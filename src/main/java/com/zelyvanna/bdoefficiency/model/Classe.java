package com.zelyvanna.bdoefficiency.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * Classe Entity
 */
@Entity
@Table(name = "classes")
public class Classe {
    /**
     * Unique identifier of the classe
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    /**
     * Class name
     * Warrior | Ranger | Sorceress | Berserker | Tamer | Musa | Maehwa | Valkyrie | Kunoichi | Ninja | Wizard | Witch | Dark Knight | Striker | Mystic | Archer | Lahn | Shai | Guardian | Hashashin | Nova | Sage | Corsair
     * All of these are available in awakening and succession version
     */
    @Column(name = "name")
    private String name;


    /**
     * Characters of the class
     */
    @JsonIgnore // This will tell jackson not to serialize that property to avoid causing the circular reference
    @OneToMany(mappedBy = "classe")
    private Set<Character> characters;


    /**
     * Empty constructor of Classe
     */
    public Classe() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }
}
