package com.zelyvanna.bdoefficiency.model;

import javax.persistence.*;

/**
 * Role Entity
 */
@Entity
@Table(name = "roles")
public class Role {
    /**
     * Unique identifier of the Role
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * Name of the Role
     */
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

    /**
     * Role empty Constructor
     */
    public Role() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ERole getName() {
        return name;
    }

    public void setName(ERole name) {
        this.name = name;
    }
}