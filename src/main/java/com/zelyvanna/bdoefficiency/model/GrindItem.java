package com.zelyvanna.bdoefficiency.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * GrindItem represents the items and their quantities dropped in a Grind session
 */
@Entity
@Table(name = "grind_items")
public class GrindItem {
    /**
     * Unique identifier of the Grind item
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Grind session linked to the Grind Item
     */
    // Grind session
    @JsonIgnore // This will tell jackson not to serialize that property to avoid causing the circular reference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grind_id")
    private Grind grind;

    /**
     * Item of the Grind Item
     */
    // Item
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    private Item item;

    /**
     * Quantity of the grind item
     */
    // Quantity
    @Column(name = "quantity")
    private int quantity;

    /**
     * Price sold of the Grind Item
     */
    // Price
    @Column(name = "price")
    private int price;

    /**
     * Grind Item's default constructor
     */
    public GrindItem() {

    }

    /**
     * Grind Item's Constructor
     *
     * @param grind    Grind of the Grind item
     * @param item     Item of the Grind item
     * @param quantity Quantity dropped
     * @param price    Price sold
     */
    public GrindItem(Grind grind, Item item, int quantity, int price) {
        this.grind = grind;
        this.item = item;
        this.quantity = quantity;
        this.price = price;
    }

    public Grind getGrind() {
        return grind;
    }

    public void setGrind(Grind grind) {
        this.grind = grind;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
