package com.zelyvanna.bdoefficiency.model;

/**
 * Enum of Roles
 */
public enum ERole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
