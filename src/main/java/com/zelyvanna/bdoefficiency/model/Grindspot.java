package com.zelyvanna.bdoefficiency.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Grindspot Entity
 */
@Entity
@Table(name = "grindspots")
public class Grindspot {
    /**
     * Unique identifier of the Grindspot
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Name of the grindspot
     */
    @Column(name = "name")
    @Size(max = 100)
    private String name;

    /**
     * Recommended Attack Points to grind at this grindspot
     */
    @Column(name = "recommended_ap")
    private int recommendedAP;

    /**
     * Grind sessions on the grindspot
     */
    @JsonIgnore
    @OneToMany(mappedBy = "grindspot")
    private Set<Grind> grinds;

    /**
     * List of obtainable items
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "grindspot_item",
            joinColumns = @JoinColumn(name = "grindspot_id"),
            inverseJoinColumns = @JoinColumn(name = "item_id"))
    private Set<Item> items = new HashSet<>();

    /**
     * Grindspot's constructor
     *
     * @param name Name of the Grindspot
     * @param recommendedAP Recommended AP of the Grindpost
     * @param items Items that you are able to drop at the grindspot
     */
    public Grindspot(@Size(max = 100) String name, int recommendedAP, Set<Item> items) {
        this.name = name;
        this.recommendedAP = recommendedAP;
        this.items = items;
    }

    /**
     * Grindspot's default constructor
     */
    public Grindspot() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRecommendedAP() {
        return recommendedAP;
    }

    public void setRecommendedAP(int recommendedAP) {
        this.recommendedAP = recommendedAP;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Set<Grind> getGrinds() {
        return grinds;
    }

    public void setGrinds(Set<Grind> grinds) {
        this.grinds = grinds;
    }
}
