package com.zelyvanna.bdoefficiency.model;

import com.zelyvanna.bdoefficiency.payload.ParameterStringBuilder;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Item Entity
 */
@Entity
@Table(name = "items",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "main_key")
        })
public class Item {
    /**
     * Unique identifier of the Item
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Name of the Item
     */
    @Column(name = "name")
    private String name;

    /**
     * Price of the Item
     */
    @Column(name = "price")
    private int price;

    /**
     * Item Icon
     */
    @Lob
    @Column(name = "icon")
    private byte[] icon;

    /**
     * Item id in the bdo API
     */
    @Column(name = "main_key")
    private int mainKey;

    /**
     * Check if the item is taxed or not
     */
    @Column(name = "taxed")
    private boolean taxed;

    /**
     * Check if the item is taxed or not
     */
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;


    /**
     * Item Empty Constructor
     */
    public Item() {
    }


    /**
     * Item's Constructor
     *
     * @param name    Name of the item
     * @param price   Price of the item
     * @param icon    icon of the item
     * @param mainKey Mainkey (bdo api key) of the item
     * @param taxed   true if the item is taxed | false
     */
    public Item(String name, int price, byte[] icon, int mainKey, boolean taxed) {
        this.name = name;
        this.price = price;
        this.icon = icon;
        this.mainKey = mainKey;
        this.taxed = taxed;
    }

    public boolean isTaxed() {
        return taxed;
    }

    public void setTaxed(boolean taxed) {
        this.taxed = taxed;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public byte[] getIcon() {
        return icon;
    }

    public void setIcon(byte[] icon) {
        this.icon = icon;
    }

    public int getMainKey() {
        return mainKey;
    }

    public void setMainKey(int mainKey) {
        this.mainKey = mainKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * Gets the marketplace price from the BDO API
     * Documentation of the API CALL : https://developers.veliainn.com/#GetMarketPriceInfo
     *
     * @param BDO_API_URL Black Desert Online API URL
     * @return The most recent marketplace price
     * @throws IOException    IOException
     * @throws ParseException ParseException
     */
    public int calculateMarketplacePrice(String BDO_API_URL) throws IOException, ParseException {
        // Build the url
        URL url = new URL(BDO_API_URL);

        // Prepare the connection
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");

        // Prepare params maps
        Map<String, Integer> parameters = new HashMap<>();
        parameters.put("keyType", 0);
        parameters.put("mainKey", this.getMainKey());
        parameters.put("subKey", 0);

        // Set params to the request
        con.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
        out.flush();
        out.close();

        // Executes the request
        int status = con.getResponseCode();

        // If the status is not -1 => the Request has been successfully executed
        if (status != -1) {
            // Read the response
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            // Extract the prices string from the content json
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(String.valueOf(content));
            String result = json.get("resultMsg").toString();

            // Create an array of prices by splitting the result
            String[] prices = result.split("-");

            // Return the most recent price
            return Integer.parseInt(prices[prices.length - 1]);
        }

        // If the Black Desert Online API is not responding properly,  we return the status code (-1)
        return status;
    }
}
