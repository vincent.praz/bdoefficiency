package com.zelyvanna.bdoefficiency.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * Character Entity
 */
@Entity
@Table(name = "characters")
public class Character {
    /**
     * Unique identifier of the Character
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * Owner of the Character
     */
    // Bidirectional many to one
    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    /**
     * Grind sessions of the character
     */
    @JsonIgnore // This will tell jackson not to serialize that property to avoid causing the circular reference
    @OneToMany(mappedBy = "c_character", cascade = CascadeType.ALL)
    private Set<Grind> grinds;

    /**
     * Class of the character
     */
    // Warrior | Ranger | Sorceress | Berserker | Tamer | Musa | Maehwa | Valkyrie | Kunoichi | Ninja | Wizard | Witch | Dark Knight | Striker | Mystic | Archer | Lahn | Shai | Guardian | Hashashin | Nova | Sage | Corsair
    @ManyToOne
    @JoinColumn(name = "classe")
    private Classe classe;

    /**
     * Name of the Character
     */
    @Column(name = "name")
    private String name;

    /**
     * Level of the Character
     */
    @Column(name = "level")
    private int level;

    /**
     * Attack Power
     */
    @Column(name = "ap")
    private int ap;

    /**
     * Awakening Attack Power
     */
    @Column(name = "aap")
    private int aap;

    /**
     * Defense Power
     */
    @Column(name = "dp")
    private int dp;

    /**
     * Character's subweapon (0 | 1 | 2 = Nouver Subweapon | Kutum TET Subweapon | Kutum PEN Subweapon)
     */
    @Column(name = "kutum")
    private int kutum;

    /**
     * Character Constructor
     *
     * @param user   Owner of the Character
     * @param name   Name of the Character
     * @param classe Class of the Character
     * @param level  Level of the Character
     * @param ap     Attack Power of the Character
     * @param aap    Awakening Attack Power of the Character
     * @param dp     Defense Power of the Character
     * @param kutum  Subweapon of the Character
     */
    public Character(User user, String name, Classe classe, int level, int ap, int aap, int dp, int kutum) {
        this.user = user;
        this.name = name;
        this.classe = classe;
        this.level = level;
        this.ap = ap;
        this.aap = aap;
        this.dp = dp;
        this.kutum = kutum;
    }

    /**
     * Default empty constructor
     */
    public Character() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getAp() {
        return ap;
    }

    public void setAp(int ap) {
        this.ap = ap;
    }

    public int getAap() {
        return aap;
    }

    public void setAap(int aap) {
        this.aap = aap;
    }

    public int getDp() {
        return dp;
    }

    public void setDp(int dp) {
        this.dp = dp;
    }

    public int getKutum() {
        return kutum;
    }

    public void setKutum(int kutum) {
        this.kutum = kutum;
    }

    public int getId() {
        return id;
    }

    public Set<Grind> getGrinds() {
        return grinds;
    }

    public void setGrinds(Set<Grind> grinds) {
        this.grinds = grinds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }
}
