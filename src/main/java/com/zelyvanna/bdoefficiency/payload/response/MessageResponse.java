package com.zelyvanna.bdoefficiency.payload.response;

/**
 * Message Response
 */
public class MessageResponse {
    private String message;

    /**
     * Message Reponse Constructor
     *
     * @param message Message of the Message Response
     */
    public MessageResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}