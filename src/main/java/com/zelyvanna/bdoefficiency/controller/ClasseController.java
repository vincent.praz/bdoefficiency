package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Classe;
import com.zelyvanna.bdoefficiency.model.ClasseCount;
import com.zelyvanna.bdoefficiency.repository.ClasseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe Controller
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ClasseController {

    @Autowired
    ClasseRepository classeRepository;

    /**
     * Returns a list of classes
     *
     * @param name the name of the classe(s) to search (if specified)
     * @return the list of classes
     */
    @GetMapping("/classes")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<Classe>> getAllClasses(@RequestParam(required = false) String name) {
        try {
            List<Classe> classes = new ArrayList<>();

            if (name == null) {
                classes.addAll(classeRepository.findAll());
            } else {
                classes.addAll(classeRepository.findByNameContaining(name));
            }

            if (classes.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(classes, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Returns a list of classes with their characters counts
     *
     * @return the list of classes and their characters counts
     */
    @GetMapping("/classes/count")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<ClasseCount>> getAllClassesWithCounts() {
        try {
            // Retrieve all classes
            List<Classe> classes = new ArrayList<>(classeRepository.findAll());

            List<ClasseCount> classeCounts = new ArrayList<>();
            // Retrieve characters from each classe
            for (Classe classe : classes) {
                classeCounts.add(new ClasseCount(classe, classe.getCharacters().size()));
            }

            if (classeCounts.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(classeCounts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows a User to see various informations about a Classe
     *
     * @param classeId the id of the Classe to show
     * @return http status OK | internal server error
     */
    @GetMapping("/classes/{classeId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Classe> showClasse(@PathVariable(value = "classeId") int classeId) {
        try {
            Classe classe = classeRepository.findById(classeId);
            return new ResponseEntity<>(classe, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
