package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Grindspot;
import com.zelyvanna.bdoefficiency.model.GrindspotCount;
import com.zelyvanna.bdoefficiency.repository.GrindspotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Grindspot Controller
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class GrindspotController {

    @Autowired
    GrindspotRepository grindspotRepository;

    /**
     * Returns a list of grindspots
     *
     * @param name the name of the grindspots(s) to search (if specified)
     * @return the list of grindspots
     */
    @GetMapping("/grindspots")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<Grindspot>> getAllGrindspots(@RequestParam(required = false) String name) {
        try {
            List<Grindspot> grindspots = new ArrayList<>();

            if (name == null) {
                grindspots.addAll(grindspotRepository.findAll());
            } else {
                grindspots.addAll(grindspotRepository.findByNameContaining(name));
            }

            if (grindspots.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(grindspots, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Allows an administrator or a moderator to add a new grindspot
     *
     * @param grindspot the grindspot recieved in parameter
     * @return the created Grindspot with the Created HttpStatus | null with the Internal server error HttpStatus
     */
    @PostMapping("/grindspots")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grindspot> addGrindspot(@RequestBody Grindspot grindspot) {
        try {
            Grindspot _grindspot = grindspotRepository
                    .save(new Grindspot(grindspot.getName(), grindspot.getRecommendedAP(), grindspot.getItems()));
            return new ResponseEntity<>(_grindspot, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows an administrator or a moderator to delete a grindspot
     *
     * @param grindspotId the id of the grindspot to delete
     * @return http status OK | internal server error
     */
    @Transactional
    @DeleteMapping("/grindspots/{grindspotId}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grindspot> deleteGrindspot(@PathVariable(value = "grindspotId") int grindspotId) {
        try {
            grindspotRepository.deleteById(grindspotId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Allows a User to see various informations about a Grindspot
     *
     * @param grindspotId the id of the grindspot to show
     * @return http status OK | internal server error
     */
    @GetMapping("/grindspots/{grindspotId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grindspot> showGrindspot(@PathVariable(value = "grindspotId") int grindspotId) {
        try {
            Grindspot grindspot = grindspotRepository.findById(grindspotId);
            return new ResponseEntity<>(grindspot, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Returns a list of Grindspots with their number of grind sessions
     *
     * @return the list of Grindspots and their number of grind sessions
     */
    @GetMapping("/grindspots/count")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<GrindspotCount>> getAllClassesWithCounts() {
        try {
            // Retrieve all grindspots
            List<Grindspot> grindspots = new ArrayList<>(grindspotRepository.findAll());

            List<GrindspotCount> grindspotCounts = new ArrayList<>();
            // Retrieve grinds from each grindspot
            for (Grindspot grindspot : grindspots) {
                grindspotCounts.add(new GrindspotCount(grindspot, grindspot.getGrinds().size()));
            }

            if (grindspotCounts.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(grindspotCounts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
