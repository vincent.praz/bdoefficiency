package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Character;
import com.zelyvanna.bdoefficiency.model.*;
import com.zelyvanna.bdoefficiency.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Grind Controller
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class GrindController {

    @Autowired
    GrindRepository grindRepository;

    @Autowired
    CharacterRepository characterRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ClasseRepository classeRepository;

    @Autowired
    GrindspotRepository grindspotRepository;

    @Autowired
    GrindItemRepository grindItemRepository;

    /**
     * Returns a list of grinds
     *
     * @param username Username of the user that will have its grinds displayed
     * @param limit    True if you want only 10 results
     * @return List of Grind Session
     */
    @GetMapping("/grinds")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<Grind>> getAllGrinds(@RequestParam(required = false) String username, @RequestParam(required = false) boolean limit) {
        try {
            List<Grind> grinds = new ArrayList<>();
            if (username == null) {
                if (limit) {
                    grinds.addAll(grindRepository.findTop10ByOrderByIdDesc());
                } else {
                    grinds.addAll(grindRepository.findAll());
                }
            } else {
                if (userRepository.findByUsername(username).isPresent()) {
                    grinds.addAll(grindRepository.findByUser(userRepository.findByUsername(username).get()));
                }
            }

            if (grinds.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(grinds, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns The money per hour (grind session) per class
     *
     * @return informations about the average money per hour per class
     */
    @GetMapping("/grinds/per_class")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<ClasseAverage>> getMoneyPerGrindSession() {
        try {
            // Retrieve all classes
            List<Classe> classes = new ArrayList<>(classeRepository.findAll());


            // List that will containt classe averages
            List<ClasseAverage> classeAverages = new ArrayList<>();

            // Retrieve characters from each classe
            for (Classe classe : classes) {


                List<Character> characters = new ArrayList<>(classe.getCharacters());
                int averagePerClasse = 0;
                int totalSilverPerHourPerClasse = 0;
                int totalGrindsPerClasse = 0;

                if (characters.size() > 0) {
                    // Retrieve grinds for each characters of each classe
                    for (Character character : characters) {
                        List<Grind> grinds = new ArrayList<>(character.getGrinds());

                        // Check if there are grinds retrieved
                        if (grinds.size() > 0) {
                            int moneyPerHour = 0;

                            for (Grind grind : grinds) {
                                // Retrieve the money per hour value of the grind hour => MONEY / TIME * 3600000 (1 hour)
                                // We divide by grinds.size to avoid integer overflow
                                if (grind.getDuration() > 0) {
                                    moneyPerHour += Math.round((double) grind.getTotalSilver() / grind.getDuration() * 3600000) / grinds.size();
                                } else {
                                    // We act as if the grind duration is an hour if there is an error and there is a 0 in database
                                    // We divide by grinds.size to avoid integer overflow
                                    moneyPerHour += grind.getTotalSilver() / grinds.size();
                                }
                            }

                            // Add number of grind to the totalGrindsPerClasse
                            totalGrindsPerClasse += grinds.size();

                            // Add Silver per hour to the totalSilverPerHourPerClasse
                            totalSilverPerHourPerClasse += moneyPerHour;
                        }
                    }
                }

                if (totalGrindsPerClasse > 0) {
                    averagePerClasse = totalSilverPerHourPerClasse;
                }

                // We add a new ClasseAverageObject to our ClasseAverages List
                classeAverages.add(new ClasseAverage(classe, averagePerClasse));
            }

            if (classeAverages.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(classeAverages, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Gets informations about the average money per hour per grindspot for the current user
     *
     * @param authentication authentication
     * @return List of GrindspotAverages
     */
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping("/grinds/average_per_user")
    public ResponseEntity<List<GrindspotAverage>> getMoneyPerGrindSessionPerGrindspotPerUser(Authentication authentication) {
        try {
            List<GrindspotAverage> grindspotAverages = new ArrayList<>();

            // Check if user exists
            if (userRepository.findByUsername(authentication.getName()).isPresent()) {
                // Retrieeve User object
                User user = userRepository.findByUsername(authentication.getName()).get();

                // Retrieve all grindspots
                List<Grindspot> grindspots = grindspotRepository.findAll();

                // For each grindspots
                for (Grindspot grindspot : grindspots) {
                    int moneyPerGrindspot = 0;

                    // Retrieve Grind sessions per grindspots for the user
                    List<Grind> grinds = grindRepository.findByUserAndGrindspot(user, grindspot);

                    // check if grinds retrieved isnt empty
                    if (grinds.size() > 0) {
                        // For each grind session of each grindspot
                        for (Grind grind : grinds) {
                            // Retrieve the money per hour value of the grind hour => MONEY / TIME * 3600000 (1 hour)
                            if (grind.getDuration() > 0) {
                                // We divide by grind size here to avoid integer overflow
                                moneyPerGrindspot += Math.round((double) grind.getTotalSilver() / grind.getDuration() * 3600000) / grinds.size();
                            } else {
                                // We act as if the grind duration is an hour if there is an error and there is a 0 in database
                                // We divide by grind size here to avoid integer overflow
                                moneyPerGrindspot += grind.getTotalSilver() / grinds.size();
                            }
                        }
                        // Add new Grindspot Average
                        grindspotAverages.add(new GrindspotAverage(grindspot, moneyPerGrindspot));
                    }
                }
                return new ResponseEntity<>(grindspotAverages, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Allows User to add a new Grind session
     *
     * @param grind the grind hour recieved in parameter
     * @return the created Grindspot with the Created HttpStatus | null with the Internal server error HttpStatus
     */
    @PostMapping("/grinds")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grind> addGrind(@RequestBody Grind grind) {
        try {
            // Create an Grind session object without the grindItems
            Grind _grind = grindRepository
                    .save(new Grind(grind.getUser(), grind.getGrindspot(), grind.getC_character(), grind.getDuration(), grind.isValuePack(), grind.isKamasylve(), grind.isDropEvent(), grind.isBlueScroll(), grind.isYellowScroll(), grind.isAgris(), grind.isTent(), grind.getNode()));

            // Prepare the empty grindItems Set
            Set<GrindItem> grindItems = new HashSet<>();

            // For each grindItem in the GrindItems in parameters of the request
            for (GrindItem grindItem : grind.getGrindItems()
            ) {
                // Set the newly created Grind sesssion to the grindItem
                grindItem.setGrind(_grind);
                // Save the _grindItem in database
                GrindItem _grindItem = grindItemRepository.save(new GrindItem(grindItem.getGrind(), grindItem.getItem(), grindItem.getQuantity(), grindItem.getPrice()));
                // Add the newly created _grindItem to the previously declared grindItems Set
                grindItems.add(_grindItem);
            }

            // We set the new grindItems Set to the _grind Session
            _grind.setGrindItems(grindItems);

            // Calculate and save the total amount of silver gained during the grind session
            _grind.setTotalSilver(_grind.calculateTotalSilver());

            // Save de _grind session in database
            _grind = grindRepository.save(_grind);

            return new ResponseEntity<>(_grind, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Allows an administrator, a moderator or the owner of the grind hour to delete the grind hour
     *
     * @param grindId        the id of the grind hour to delete
     * @param authentication Authentication Request
     * @return http status OK | internal server error
     */
    @Transactional
    @DeleteMapping("/grinds/{grindId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grindspot> deleteGrind(@PathVariable(value = "grindId") int grindId, Authentication authentication) {
        Grind grind = grindRepository.findById(grindId);

        // Check access rights
        if (authentication.getName().equals(grind.getUser().getUsername()) || authentication.getAuthorities().toString().indexOf(ERole.ROLE_ADMIN.toString()) > 0) {
            try {
                grindRepository.deleteById(grindId);
                return new ResponseEntity<>(HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);

    }


    /**
     * Allows User to view informations about a grind hour
     *
     * @param grindId the id of the grind hour to show
     * @return http status OK | internal server error
     */
    @GetMapping("/grinds/{grindId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grind> showGrind(@PathVariable(value = "grindId") int grindId) {
        try {
            Grind grind = grindRepository.findById(grindId);
            return new ResponseEntity<>(grind, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows a User to edit various informations about his Grind session
     *
     * @param grind          The new grind datas
     * @param grindId        Unique identifier of the grind to edit
     * @param authentication Authentication Request
     * @return Grind entity + status code
     */
    @Transactional
    @PutMapping("/grinds/{grindId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grind> updateGrind(@RequestBody Grind grind, @PathVariable(value = "grindId") int grindId, Authentication authentication) {
        // Retrieve old grind session
        Grind oldGrind = grindRepository.findById(grindId);

        try {
            // 1 : WE EDIT / REMOVE THE OLD ITEMS
            if (oldGrind.getGrindItems().size() > 0) {
                for (GrindItem grindItem : oldGrind.getGrindItems()) {
                    // Check if item has been edited
                    for (GrindItem newGrindItem : grind.getGrindItems()) {
                        if (grindItem.getItem().getId() == newGrindItem.getItem().getId()) {
                            //Check new quantity
                            if (newGrindItem.getQuantity() == 0) {
                                // Remove from the grinditems list
                                oldGrind.removeGrindItem(grindItem);

                                // Delete the grinditem if the grinditem's new qty is 0
                                grindItemRepository.deleteById(grindItem.getId());

                            } else {
                                // Set new price and qty values
                                grindItem.setQuantity(newGrindItem.getQuantity());
                                grindItem.setPrice(newGrindItem.getPrice());
                                // Save the grinditem object
                                grindItemRepository.save(grindItem);
                            }
                        }
                    }
                }
            }

            // 2 : WE ADD THE OTHER ITEMS THAT WERE NOT IN THE GRIND ALREADY
            for (GrindItem newGrindItem : grind.getGrindItems()) {
                boolean found = false;
                for (GrindItem grindItem : oldGrind.getGrindItems()) {
                    // Check if the newItem already exists in the grind
                    if (grindItem.getItem().getId() == newGrindItem.getItem().getId()) {
                        found = true;
                        break;
                    }
                }

                // If the item hasn't been found, that means its a new item to add to our old grind session
                if (!found) {
                    newGrindItem.setGrind(oldGrind);
                    newGrindItem = grindItemRepository.save(newGrindItem);

                    // Add new grind item
                    oldGrind.addGrindItem(newGrindItem);
                }
            }

            // Set grind properties
            oldGrind.setGrindspot(grind.getGrindspot());
            oldGrind.setC_character(grind.getC_character());
            oldGrind.setAgris(grind.isAgris());
            oldGrind.setBlueScroll(grind.isBlueScroll());
            oldGrind.setYellowScroll(grind.isYellowScroll());
            oldGrind.setDropEvent(grind.isDropEvent());
            oldGrind.setDuration(grind.getDuration());
            oldGrind.setKamasylve(grind.isKamasylve());
            oldGrind.setValuePack(grind.isValuePack());
            oldGrind.setNode(grind.getNode());
            oldGrind.setTent(grind.isTent());
            oldGrind.setTotalSilver(grind.calculateTotalSilver());

            // Check if the character exists and the character user is the same as the user that tries to edit it
            if (authentication.getName().equals(oldGrind.getUser().getUsername()) || authentication.getAuthorities().toString().indexOf(ERole.ROLE_ADMIN.toString()) > 0) {
                return new ResponseEntity<>(grindRepository.save(oldGrind), HttpStatus.OK);
            }

            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            // Avoid exceptions with empty beans handler
            if (e.getMessage() == null) {
                if (authentication.getName().equals(oldGrind.getUser().getUsername()) || authentication.getAuthorities().toString().indexOf(ERole.ROLE_ADMIN.toString()) > 0) {
                    return new ResponseEntity<>(grindRepository.save(oldGrind), HttpStatus.OK);
                }
            }
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
