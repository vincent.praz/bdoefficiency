package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Item;
import com.zelyvanna.bdoefficiency.repository.ItemRepository;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Item Controller
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ItemController {

    @Autowired
    ItemRepository itemRepository;

    // We retrieve the BDO_API_URL Variable from the App properties
    @Value("${bdoefficiency.app.BDO_API_URL}")
    private String BDO_API_URL;

    /**
     * Returns a list of items
     *
     * @param name the name of the item(s) to search (if specified)
     * @return the list of items
     */
    @GetMapping("/items")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<Item>> getAllItems(@RequestParam(required = false) String name) {
        try {
            List<Item> items = new ArrayList<>();

            if (name == null) {
                items.addAll(itemRepository.findAll());
            } else {
                items.addAll(itemRepository.findByNameContaining(name));
            }

            if (items.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(items, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Calls the Black Desert Online Official API and retrieve the In Game price of the item
     *
     * @param itemId Unique identifier of the item that we want to update
     * @return The Status + item with the new price
     * @throws IOException    IOException
     * @throws ParseException ParseException
     */
    @PatchMapping("/items/{itemId}/recalculate")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Item> recalculateItemPrice(@PathVariable(value = "itemId") int itemId) throws IOException, ParseException {
        if (itemId != 0) {
            // Retrieve the item from the database
            Item item = itemRepository.findById(itemId);

            recalculateItem(item);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            List<Item> items = new ArrayList<>(itemRepository.findAll());
            for (Item item : items) {
                recalculateItem(item);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }


    /**
     * Calls the Black Desert Online Official API and retrieve the In Game price of the item
     *
     * @param item the item that we want to update
     * @throws IOException    IOException
     * @throws ParseException ParseException
     */
    public void recalculateItem(Item item) throws IOException, ParseException {
        if (item != null && item.isTaxed()) {
            // Current time
            Date now = new Date();

            // Date of the last Item update
            Date itemDate = item.getUpdatedAt();

            // Check if the last item update is Null to avoid null pointers
            // We check if the last price update was less than 5 minutes ago to avoid spamming the BDO API
            if (itemDate != null && now.getTime() - itemDate.getTime() <= 5 * 60 * 1000) {
                return;
            }

            // Recalculate the item marketplace price
            int newPrice = item.calculateMarketplacePrice(BDO_API_URL);

            // We also check if the newPrice is > 0 in order to  avoid saving the new price
            // if the calculatMarketplacePrice returns -1
            if (newPrice > 0) {
                // We check if the new price is different from the old one to avoid unnecessary field queries
                if (newPrice != item.getPrice()) {
                    item.setPrice(newPrice);
                }
                // Set the price update DateTime to now
                item.setUpdatedAt(now);
                itemRepository.save(item);
            }
        }
    }

    /**
     * Allows an administrator or a moderator to add a new item
     *
     * @param iconFile Icon of the tiem
     * @param name     Name of the item
     * @param price    Price of the item
     * @param mainKey  BDO API KEY of the item
     * @param taxed    boolean false item not taxed | true item taxed
     * @return the created Item with the Created HttpStatus | null with the Internal server error HttpStatus
     */
    @PostMapping("/items")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Item> addItem(@RequestParam("iconFile") MultipartFile iconFile, @RequestParam("name") String name, @RequestParam("price") int price, @RequestParam("mainKey") int mainKey, @RequestParam("taxed") boolean taxed) {
        try {
            Item _item = itemRepository
                    .save(new Item(name, price, iconFile.getBytes(), mainKey, taxed));
            return new ResponseEntity<>(_item, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows an administrator or a moderator to delete an item
     *
     * @param itemId the id of the item to delete
     * @return http status OK | internal server error
     */
    @Transactional
    @DeleteMapping("/items/{itemId}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Item> deleteItem(@PathVariable(value = "itemId") int itemId) {
        try {
            itemRepository.deleteById(itemId);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
