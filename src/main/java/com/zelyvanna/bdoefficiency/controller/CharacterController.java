package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Character;
import com.zelyvanna.bdoefficiency.model.ERole;
import com.zelyvanna.bdoefficiency.model.Grindspot;
import com.zelyvanna.bdoefficiency.repository.CharacterRepository;
import com.zelyvanna.bdoefficiency.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Character Controller
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class CharacterController {

    @Autowired
    CharacterRepository characterRepository;

    @Autowired
    UserRepository userRepository;

    /**
     * Returns a list of characters
     *
     * @param name     the name of the characters(s) to search (if specified)
     * @param username the User of the characters(s) to search (if specified)
     * @return the list of characters
     */
    @GetMapping("/characters")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<Character>> getAllCharacters(@RequestParam(required = false) String name, @RequestParam(required = false) String username) {
        try {
            List<Character> characters = new ArrayList<>();

            if (name == null && username == null) {
                characters.addAll(characterRepository.findAll());
            } else if (username != null && name == null) {
                // Get all characters owned by the user
                if (userRepository.findByUsername(username).isPresent()) {
                    characters.addAll(characterRepository.findByUser(userRepository.findByUsername(username).get()));
                }
            } else {
                characters.addAll(characterRepository.findByNameContaining(name));
            }

            if (characters.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(characters, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows an administrator or a moderator or a user to add a new character
     *
     * @param character the character recieved in parameter
     * @return the created Grindspot with the Created HttpStatus | null with the Internal server error HttpStatus
     */
    @PostMapping("/characters")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        try {
            Character _character = characterRepository
                    .save(new Character(character.getUser(), character.getName(), character.getClasse(), character.getLevel(), character.getAp(), character.getAap(), character.getDp(), character.getKutum()));
            return new ResponseEntity<>(_character, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows an administrator or a moderator to delete a Character
     *
     * @param characterId the id of the Character to delete
     * @param authentication Authentication Request
     * @return http status OK | internal server error
     * @throws IOException IOException
     */
    @Transactional
    @DeleteMapping("/characters/{characterId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Grindspot> deleteCharacter(@PathVariable(value = "characterId") int characterId, Authentication authentication) throws IOException {
        try {
            Character character = characterRepository.findById(characterId);

            // Check if the user deleting is the same as the owner or if it is an administrator
            if (authentication.getName().equals(character.getUser().getUsername()) || authentication.getAuthorities().toString().indexOf(ERole.ROLE_ADMIN.toString()) > 0) {
                characterRepository.deleteById(characterId);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows a User to see various informations about a Character
     *
     * @param characterId the id of the Character to show
     * @return http status OK | internal server error
     */
    @GetMapping("/characters/{characterId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Character> showCharacter(@PathVariable(value = "characterId") int characterId) {
        try {
            Character character = characterRepository.findById(characterId);
            return new ResponseEntity<>(character, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Allows a User to edit various informations about his Character
     *
     * @param character   The new characters data
     * @param characterId the character's unique identifier
     * @param authentication Authentication request
     * @return The updated character + http status
     */
    @PutMapping("/characters/{characterId}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<Character> updateCharacter(@RequestBody Character character, @PathVariable(value = "characterId") int characterId, Authentication authentication) {
        try {
            // Retrieve old character
            Character oldCharacter = characterRepository.findById(characterId);

            // Create a new character object with the given datas
            Character editedCharacter = oldCharacter;
            editedCharacter.setName(character.getName());
            editedCharacter.setClasse(character.getClasse());
            editedCharacter.setLevel(character.getLevel());
            editedCharacter.setDp(character.getDp());
            editedCharacter.setAap(character.getAap());
            editedCharacter.setAp(character.getAp());
            editedCharacter.setKutum(character.getKutum());


            // Check if the user editing is the same as the owner or if it is an administrator
            if (authentication.getName().equals(oldCharacter.getUser().getUsername()) || authentication.getAuthorities().toString().indexOf(ERole.ROLE_ADMIN.toString()) > 0) {
                return new ResponseEntity<>(characterRepository.save(editedCharacter), HttpStatus.OK);
            }

            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
