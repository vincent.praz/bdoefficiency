package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Character;
import com.zelyvanna.bdoefficiency.repository.CharacterRepository;
import com.zelyvanna.bdoefficiency.security.TestSecurityConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestSecurityConfig.class)
@AutoConfigureMockMvc
class CharacterControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CharacterRepository characterRepository;


    @Test
    void getAllCharacters() throws Exception {
        mockMvc.perform(get("/characters")).andExpect(status().isOk()).andExpect(content().string(containsString("Zelyvanna")));
    }

    @Test
    void showCharacter() throws Exception {
        Character character = characterRepository.findById(36);
        mockMvc.perform(get("/characters?name=Zelyvanna")).andExpect(status().isOk()).andExpect(content().string(containsString(character.getName())));
    }

}