package com.zelyvanna.bdoefficiency.controller;

import com.zelyvanna.bdoefficiency.model.Classe;
import com.zelyvanna.bdoefficiency.repository.ClasseRepository;
import com.zelyvanna.bdoefficiency.security.TestSecurityConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestSecurityConfig.class)
@AutoConfigureMockMvc
class ClasseControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private ClasseRepository classeRepository;

    @Test
    void getAllClasses() throws Exception {
        // We get the Sorceress Awakening class and test if it class is present in the results
        mockMvc.perform(get("/classes?name=Sorceress Awakening")).andExpect(status().isOk()).andExpect(content().string(containsString("Sorceress Awakening")));
    }

    @Test
    void showClasse() throws Exception {
        Classe classe = classeRepository.findById(2);
        mockMvc.perform(get("/classes?name=Sorceress Awakening")).andExpect(status().isOk()).andExpect(content().string(containsString(classe.getName())));
    }
}